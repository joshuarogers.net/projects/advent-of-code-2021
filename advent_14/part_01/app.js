const fs = require('fs');
const _ = require('lodash');

const [initialPolymer, rawPolymerRules] = fs.readFileSync('input.txt')
    .toString()
    .split('\n\n')
    .map(x => x.trim());

const polymerRules = _.fromPairs(rawPolymerRules.split('\n')
    .map(x => x.split(' -> ')));

const pairs = line => _.range(0, line.length - 1)
    .map(x => line.slice(x, x + 2));

const growPolymer = polymer => {
    const insertions = pairs(polymer).map(x => polymerRules[x]);
    return _.flatten(_.zip(polymer.split(''), insertions)).join('');
};

const stepCount = 10;

const finalPolymer = _.range(0, stepCount)
    .reduce(growPolymer, initialPolymer);

const componentFrequencies =  _(finalPolymer.split(''))
    .groupBy(x => x)
    .toPairs()
    .map(([_id, matches]) => [_id, matches.length])
    .orderBy(([_id, matches]) => matches, "desc")
    .value();

const score = _.first(componentFrequencies)[1] - _.last(componentFrequencies)[1];

console.log(score);
