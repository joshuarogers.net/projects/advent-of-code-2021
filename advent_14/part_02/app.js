const fs = require('fs');
const _ = require('lodash');

const [initialPolymer, rawPolymerRules] = fs.readFileSync('input.txt')
    .toString()
    .split('\n\n')
    .map(x => x.trim());

const polymerRules = _.fromPairs(rawPolymerRules.split('\n')
    .map(x => x.split(' -> ')));

const pairs = line => _.range(0, line.length - 1)
    .map(x => line.slice(x, x + 2));

const frequencies = line => _(line.split(''))
    .groupBy()
    .toPairs()
    .map(([key, values]) => [key, values.length])
    .fromPairs()
    .value();

const sumFrequencies = frequencies => _(frequencies)
    .map(_.toPairs)
    .flatten()
    .groupBy(([key, _value]) => key)
    .toPairs()
    .map(([key, nestedvalues]) => [ key, _.sum(nestedvalues.map(([_key, value]) => value)) ])
    .fromPairs()
    .value();

// The easiest way for me to get the frequency here was to use the logic that the final frequency
// count will be the sum of the frequency starting this iteration and the frequency of the items
// inserted. Each step returns the frequncy of the inserted items and then adds its own before
// returning.
const growPolymer = (polymer, targetStepCount) => {
    const growPolymerStep = _.memoize((stepPolymer, currentStepCount) => {
        if (currentStepCount === targetStepCount)
            return {};

        const insertions = pairs(stepPolymer).map(x => polymerRules[x]);
        const grownPolymer = _(stepPolymer.split(''))
            .zip(insertions)
            .flatten()
            .join('');

        const result = sumFrequencies(pairs(grownPolymer)
            .map(x => growPolymerStep(x, currentStepCount + 1)));

        return sumFrequencies([result, frequencies(insertions.join(''))]);
    }, (stepPolymer, currentStepCount) => `${stepPolymer}:${currentStepCount}`);

    return sumFrequencies(pairs(polymer)
        .map(x => growPolymerStep(x, 0))
        .concat([frequencies(polymer)]));
};

const finalPolymer = growPolymer(initialPolymer, 40);
const componentFrequencies = _(finalPolymer)
    .toPairs()
    .orderBy(([key, value]) => value, "desc")
    .map(([_key, value]) => value)
    .value();

console.log(_.first(componentFrequencies) - _.last(componentFrequencies));