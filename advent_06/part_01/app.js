const fs = require('fs');
const _ = require('lodash');

const initialState = fs.readFileSync('input.txt').toString().trim().split(',').map(x => parseInt(x));
const simulationDays = 80;

const simulateDay = input => _(input)
    .map(x => x > 0 ? x - 1 : [6, 8])
    .flatten()
    .value();

const finalState = _.range(0, simulationDays)
    .reduce(simulateDay, initialState);

console.log(finalState.length);