const fs = require('fs');
const _ = require('lodash');

const initialInternalTimer = 8;
const postReproductionInternalTimer = 6;
const simulationDays = 256;

const initialState = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split(',')
    .map(x => parseInt(x));

const countPerAge = _.range(0, initialInternalTimer + 1)
    .map(i => initialState.filter(x => x === i).length);

const finalCountPerAge = _.range(0, simulationDays).reduce(([readyNow, ...rest]) => {
    // The number of lanternfish with 0 time remaining should be added to the quantity of newly
    // born laternfish and to the quantity of who still have 6 days remaining before another reproduction.
    rest.push(readyNow);
    rest[postReproductionInternalTimer] += readyNow;
    return rest;
}, countPerAge);

console.log(_.sum(finalCountPerAge));