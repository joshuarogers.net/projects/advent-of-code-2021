const fs = require('fs');
const _ = require('lodash');

const rawInput = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');

const pivotTable = table => _.range(table.length).map(x => table.map(y => y[x]));
const createCard = cells => {
    let possibilities = cells.concat(pivotTable(cells));

    return {
        cells,
        tryMark: number => {
            possibilities = possibilities.map(possibility => possibility.filter(x => x !== number))
        },
        remainingCells: () => _(possibilities).flatten().uniq().value(),
        hasBingo: () => possibilities.filter(x => x.length === 0).length > 0
    }
}

const parseBingoState = text => {
    const [ballsDto, ...cardsDto] = text.filter(x => x !== '');

    const parseCardRow = row => row
        .split(' ')
        .filter(x => x !== '')
        .map(x => parseInt(x));
    const parseCard = rows => rows.map(parseCardRow);

    const balls = ballsDto.split(',').map(x => parseInt(x));
    const reverseBalls = _.reverse(balls);

    return {
        getBall: () => reverseBalls.pop(),
        cards: _.chunk(cardsDto, 5).map(parseCard).map(createCard)
    };
};

const { getBall, cards } = parseBingoState(rawInput);

let remainingCards = [...cards];
while (true) {
    const ball = getBall();

    for (let card of remainingCards)
        card.tryMark(ball);

    if (remainingCards.length === 1 && remainingCards[0].hasBingo()) {
        console.log(_.sum(remainingCards[0].remainingCells()) * ball);
        break;
    }

    remainingCards = remainingCards.filter(x => !x.hasBingo());
}