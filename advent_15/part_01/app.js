const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .map(y => y.split('').map(x => parseInt(x)));
const gridSize = {x: _.first(input).length, y: input.length};

const weightedQueue = costFn => {
    const items = new Set();

    return {
        queue: value => items.add({ value: value, cost: costFn(value) }),
        dequeue: () => {
            let bestItem = null;
            items.forEach(item => {
                if (bestItem === null || bestItem.cost > item.cost)
                    bestItem = item;
            });
            items.delete(bestItem);
            return bestItem.value;
        },
        hasNext: () => !items.values().next().done
    }
};

const sumRisk = (riskGrid, path) => _.sum(path.map(([x, y]) => riskGrid[y][x]));

const trace = (riskGrid, start, [endX, endY]) => {
    const cellId = ([x, y]) => (y * gridSize.y) + x;
    const visitedCells = new Set([cellId(start)]);

    const candidateQueue = weightedQueue(path => sumRisk(riskGrid, path));
    candidateQueue.queue([start]);

    const neighborOffsets = [
        [-1, 0],
        [1,  0],
        [0, -1],
        [0,  1]
    ];

    while (candidateQueue.hasNext()) {
        const nextPath = candidateQueue.dequeue();
        const [x, y] = _.last(nextPath);

        if (x === endX && y === endY)
            return nextPath;

        const unvisitedNeighbors = neighborOffsets
            .map(([dx, dy]) => [x + dx, y + dy])
            .filter(neighbor => !visitedCells.has(cellId(neighbor)))
            .filter(([neighborX, neighborY]) => neighborX >= 0 && neighborY >= 0 && neighborX < gridSize.x && neighborY < gridSize.y);

        for (const unvisitedNeighbor of unvisitedNeighbors) {
            candidateQueue.queue([...nextPath, unvisitedNeighbor]);
            visitedCells.add(cellId(unvisitedNeighbor));
        }
    }

    console.log("No path found");
    return null;
}

const startPosition = [0, 0];
const targetPosition = [gridSize.x - 1, gridSize.y - 1];
const path = trace(input, startPosition, targetPosition);

const risk = sumRisk(input, path.slice(1));

console.log(risk);