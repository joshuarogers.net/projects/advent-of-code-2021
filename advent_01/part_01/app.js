const _ = require("lodash");
const fs = require("fs");

const rawInput = _.reverse(fs.readFileSync("input.txt").toString().trim().split('\n'));

const increasingPairCount = _.zip(rawInput, rawInput.slice(1)).filter(([x, y]) => x < y).length;
console.log(increasingPairCount);

// This produces an answer too low by 1