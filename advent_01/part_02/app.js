const _ = require("lodash");
const fs = require("fs");

const rawInput = fs.readFileSync("input.txt").toString().trim().split('\n').map(x => parseInt(x));
const rollingSums = _.zip(rawInput, rawInput.slice(1), rawInput.slice(2)).map(_.sum);
const increasingPairCount = _.zip(rollingSums, rollingSums.slice(1)).filter(([x, y]) => x < y).length;
console.log(increasingPairCount);
