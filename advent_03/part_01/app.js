const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt').toString().trim().split('\n');
const binaryNumberLength = _.first(input).length;

const frequencies = list => {
    return _(list)
        .groupBy()
        .toPairs()
        .map(([name, instances]) => [name, instances.length])
        .fromPairs()
        .value();
}

const mostCommonBit = (list, pos) => {
    const digitFrequencies = frequencies(list.map(x => x[pos]));
    return (digitFrequencies['0'] > digitFrequencies['1'])
        ? 0
        : 1;
}

const invert = binaryDigit => _.map(binaryDigit, x => x === '1' ? '0' : '1').join('');
const mostCommonBinaryNumber = _.range(binaryNumberLength).map(x => mostCommonBit(input, x)).join('');
const leastCommonBinaryNumber = invert(mostCommonBinaryNumber);
console.log(parseInt(mostCommonBinaryNumber, 2) * parseInt(leastCommonBinaryNumber, 2));