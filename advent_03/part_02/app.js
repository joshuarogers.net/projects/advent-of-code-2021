const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt').toString().trim().split('\n');
const binaryNumberLength = _.first(input).length;

const frequencies = list => {
    return _(list)
        .groupBy()
        .toPairs()
        .map(([name, instances]) => [name, instances.length])
        .fromPairs()
        .value();
}

const mostCommonBit = (list, pos) => {
    const digitFrequencies = frequencies(list.map(x => x[pos]));

    if (digitFrequencies['0'] === digitFrequencies['1'])
        return null;

    return (digitFrequencies['0'] > digitFrequencies['1'])
        ? '0'
        : '1';
}

const getOxygenGeneratorRating = (allNumbers) => {
    const recur = (numbers, index) => {
        const commonBit = mostCommonBit(numbers, index) ?? '1';
        const filteredNumbers = numbers.filter(x => x[index] === commonBit);

        return filteredNumbers.length > 1
            ? recur(filteredNumbers, index + 1)
            : filteredNumbers[0];
    };

    return recur(allNumbers, 0);
}

const getCO2ScrubberRating = (allNumbers) => {
    const recur = (numbers, index) => {
        const commonBit = mostCommonBit(numbers, index) ?? '1';
        const filteredNumbers = numbers.filter(x => x[index] !== commonBit);

        return filteredNumbers.length > 1
            ? recur(filteredNumbers, index + 1)
            : filteredNumbers[0];
    };

    return recur(allNumbers, 0);
}

const oxygenGeneratorRating = getOxygenGeneratorRating(input);
const co2ScrubberRating = getCO2ScrubberRating(input);
console.log(parseInt(oxygenGeneratorRating, 2) * parseInt(co2ScrubberRating, 2));
