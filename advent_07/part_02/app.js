const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split(',')
    .map(x => parseInt(x));

const maxValue = _.max(input);
console.log(_(_.range(0, maxValue + 1))
    .map(i => [i, _(input).map(x => _.sum(_.range(0, Math.abs(x - i) + 1))).sum()])
    .sortBy(([_i, sum]) => sum)
    .first()[1]);
