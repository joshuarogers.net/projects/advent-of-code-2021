const fs = require('fs');
const _ = require('lodash');

const [minX, maxX, minY, maxY] = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .match(/target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)/)
    .splice(1, 5)
    .map(x => parseInt(x));

const outOfBounds = probe => probe.y < minY || probe.x > maxX;

const inTargetZone = probe => probe.x >= minX && probe.x <= maxX && probe.y >= minY && probe.y <= maxY;

const doTick = probe => {
    probe.x += probe.dx;
    probe.y += probe.dy;
    probe.dx = Math.max(0, probe.dx - 1);
    probe.dy--;
}

const isValidProbeTrajectory = ([dx, dy]) => {
    const probe = { x: 0, y: 0, dx, dy };
    
    while (!outOfBounds(probe)) {
        if (inTargetZone(probe))
            return true;
        doTick(probe);
    }

    return false;
}

const trajectories = _.flatten(_.range(-Math.abs(minY) - 1, Math.abs(minY) + 2)
    .map(dy => _.range(0, maxX + 1).map(dx => [dx, dy])));

console.log(trajectories.filter(isValidProbeTrajectory).length);
