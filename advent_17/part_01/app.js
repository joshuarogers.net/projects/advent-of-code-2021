// Look, I could write an application to solve this one, but I don't really see the point:
// Judging by the width of the target area (the X is less than sum(0..WIDTH)) so it should
// be possible for me to come up with an X where the drag will eventually make the dx 0
// within the area. Given that, I should be able to choose the largest dy that will get me
// inside the bounds. Since the effect of gravity is a constant (-1) and we are firing up,
// the dy will be parabolic(-ish). So, the highest dy that will get us in the target is the
// y of the bottom abs(position) - 1. Given that and given that the dy decreases by one each
// step, the highest value will be sum(0..abs(lowestY) - 1). So I'll just write the loop for
// that and call it done.

let result = 0;
for (let i = 0; i < 80; i++)
    result += i;

console.log(result);