const fs = require('fs');
const _ = require('lodash');

const inputLines = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');

const matchingPairs = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
};

const autoCompleteCharacterScores = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
};

/**
 * Returns the index of the first syntax error, or -1 if no errors exist
 */
const getAutoCompleteSuggestion = line => {
    const matchingPairStack = [];

    for (let i = 0; i < line.length; i++) {
        const currentChar = line[i];

        if (matchingPairs[currentChar])
            matchingPairStack.push(matchingPairs[currentChar]);
        else if (currentChar !== matchingPairStack.pop())
            return [];
    }

    return _.reverse(matchingPairStack).join('');
};

const getScore = suggestion => _.reduce(suggestion, (score, char) => score * 5 + autoCompleteCharacterScores[char], 0);

const scores = _(inputLines)
    .map(getAutoCompleteSuggestion)
    .map(getScore)
    .filter(x => x > 0)
    .orderBy(x => x)
    .value();

console.log(scores[Math.floor(scores.length / 2)]);
