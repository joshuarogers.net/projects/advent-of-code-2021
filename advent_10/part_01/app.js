const fs = require('fs');
const _ = require('lodash');

const inputLines = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');

const matchingPairs = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>'
};

const invalidCharacterScores = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

/**
 * Returns the index of the first syntax error, or -1 if no errors exist
 */
const findSyntaxErrorLocation = line => {
    const matchingPairStack = [];

    for (let i = 0; i < line.length; i++) {
        const currentChar = line[i];

        if (matchingPairs[currentChar])
            matchingPairStack.push(matchingPairs[currentChar]);
        else if (currentChar !== matchingPairStack.pop())
            return i;
    }

    return -1;
};

const getScore = (line, position) => position >= 0
    ? invalidCharacterScores[line[position]]
    : 0;

const score = _(inputLines)
    .map(x => getScore(x, findSyntaxErrorLocation(x)))
    .sum();

console.log(score);