const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .map(x => x.trim().split('|').map(y => y.trim().split(' ')));

const normalizeKey = key => _.orderBy(key.split(''), x => x).join('');

const buildKey = wires => {
    const numbersBySegmentCount = _.groupBy(wires.map(normalizeKey), x => x.length);

    const knownMappings = new Array(10);

    // These we can determine just based on the number of segments
    knownMappings[1] = numbersBySegmentCount['2'][0];
    knownMappings[4] = numbersBySegmentCount['4'][0];
    knownMappings[7] = numbersBySegmentCount['3'][0];
    knownMappings[8] = numbersBySegmentCount['7'][0];

    // 3 is the only 5 segment number that is a superset of the segments of 7.
    knownMappings[3] = numbersBySegmentCount['5'].find(x => _.difference(knownMappings[7].split(''), x.split('')).length === 0);

    // 9 is the only 6 segment number that is a superset of the segments of 4.
    knownMappings[9] = numbersBySegmentCount['6'].find(x => _.difference(knownMappings[4].split(''), x.split('')).length === 0);

    // 5 is the only 6 segment number that is a subset of the segments of 9 other than 3.
    knownMappings[5] = numbersBySegmentCount['5'].find(x => x !== knownMappings[3] && _.difference(x.split(''), knownMappings[9].split('')).length === 0);

    // 2 is the only remaining 5 segment number.
    knownMappings[2] = _.without(numbersBySegmentCount[5], knownMappings[3], knownMappings[5])[0];

    // 0 is the only remaining 6 segment number that is a superset of the segments of 7.
    knownMappings[0] = numbersBySegmentCount['6'].find(x => x !== knownMappings[9] &&  _.difference(knownMappings[7].split(''), x.split('')).length === 0);

    // 6 is the only remaining 6 segment number.
    knownMappings[6] = _.without(numbersBySegmentCount['6'], knownMappings[0], knownMappings[9])[0];

    return _.invert(knownMappings);
}

const results = input.map(line => {
    const key = buildKey(line[0]);
    return parseInt(line[1].map(x => key[normalizeKey(x)]).join(''));
})

console.log(_.sum(results));