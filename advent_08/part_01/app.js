const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');

const segments = input.map(x => x.trim()
    .split('|')[1]
    .trim()
    .split(' '));

const knownSegmentLengths = new Set([2, 3, 4, 7]);
console.log(_.flatten(segments).filter(x => knownSegmentLengths.has(x.length)).length);