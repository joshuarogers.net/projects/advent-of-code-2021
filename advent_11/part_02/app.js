const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .map(y => _.map(y, x => parseInt(x)));

const gridMap = (grid, fn) => grid.map((row, y) => row.map((item, x) => fn(item, [x, y])));

// Save state processing
const jellyfishTemplate = { energyLevel: 0, highlightCount: 0 };
const initialJellyfishState = gridMap(input, x => ({ ...jellyfishTemplate, energyLevel: x }));

// Neighbors
const gridSize = 10;
const highlightOffsets = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [-1, 1], [0, 1], [1, 1]];
const getNeighbors = ([x, y]) => highlightOffsets
    .map(([dx, dy]) => [x + dx, y  + dy])
    .filter(([x, y]) => x >= 0 && y >= 0 && x < gridSize && y < gridSize);

// Simulation steps
const stepStateIncreaseEnergy = state => gridMap(state, jellyfish => ({
    ...jellyfish,
    energyLevel: jellyfish.energyLevel + 1 
}));

const jellyfishMaxEnergy = 9;
const stepStatePropogateHighlights = state => {
    const workingState = gridMap(state, x => ({ ...x }));
    const highlightQueue = _(gridMap(workingState, (jellyfish, [x, y]) => jellyfish.energyLevel > jellyfishMaxEnergy ? [x, y] : null))
        .flatten()
        .filter(x => x !== null)
        .value();

    while (highlightQueue.length > 0) {
        const neighors = getNeighbors(highlightQueue.pop());

        for (const [x, y] of neighors) {
            const neighbor = workingState[y][x];

            // A highlight has already occurred this step, so skip it.
            if (neighbor.energyLevel > jellyfishMaxEnergy)
                continue;
            
            // A highlight has just occurred and must be processed
            neighbor.energyLevel++;
            if (neighbor.energyLevel > jellyfishMaxEnergy)
                highlightQueue.push([x, y]);
        }
    }

    return workingState;
};

const stepClampValues = state => gridMap(state, jellyfish =>
    jellyfish.energyLevel > jellyfishMaxEnergy
        ? { energyLevel: 0, highlightCount: jellyfish.highlightCount + 1 }
        : jellyfish);

// This line is a perfect reason why I hope the ECMAScript pipe syntax gets approved.
const stepState = state => stepClampValues(stepStatePropogateHighlights(stepStateIncreaseEnergy(state)));

const stepUntil = (state, fn) => {
    let stepCount = 0;
    while (!fn(state)) {
        state = stepState(state);
        stepCount++;
    }

    return { stepCount, state };
}

const isSimultaneousHighlight = state => _(state).flatten().every(jellyfish => jellyfish.energyLevel === 0)
console.log(stepUntil(initialJellyfishState, isSimultaneousHighlight).stepCount);
