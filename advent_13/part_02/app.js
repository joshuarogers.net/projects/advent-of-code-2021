const fs = require('fs');
const _ = require('lodash');

const [inputPoints, inputFolds] = fs.readFileSync('input.txt')
    .toString()
    .split('\n\n');

const points = inputPoints.split('\n')
    .map(line => line.split(',').map(x => parseInt(x)));

const folds = inputFolds.trim()
    .split('\n')
    .map(x => x.split(' ')[2])
    .map(x => x.split('='))
    .map(([axis, position]) => ({ axis, 'foldPosition': parseInt(position) }));

const foldFns = {
    'x': ([x, y], position) => [position - (Math.abs(x - position)), y],
    'y': ([x, y], position) => [x, position - (Math.abs(y - position))]
}

const results = folds.reduce((state, {axis, foldPosition}) => state.map(coordinate => foldFns[axis](coordinate, foldPosition)), points);
const maxX = _.max(results.map(x => x[0]));
const maxY = _.max(results.map(x => x[1]));

const outputGrid = _.range(0, maxY + 1).map(() => new Array(maxX + 1).fill(' '));
for (const [x, y] of results)
    outputGrid[y][x] = '#';

const outputText = outputGrid.map(row => row.join('')).join('\n');
console.log(outputText);