const fs = require('fs');
const _ = require('lodash');

const [inputPoints, inputFolds] = fs.readFileSync('input.txt')
    .toString()
    .split('\n\n');

const points = inputPoints.split('\n')
    .map(line => line.split(',').map(x => parseInt(x)));

const [axis, positionText] = inputFolds.split('\n')[0]
    .split(' ')[2]
    .split('=');

const foldFns = {
    'x': ([x, y], position) => [position - (Math.abs(x - position)), y],
    'y': ([x, y], position) => [x, position - (Math.abs(y - position))]
}

const position = parseInt(positionText);
const results = _(points)
    .map(point => foldFns[axis](point, position))
    .map(([x, y]) => `${x}, ${y}`)
    .uniq()
    .value()
    .length;

console.log(results);