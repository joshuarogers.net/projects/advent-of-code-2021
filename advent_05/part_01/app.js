const fs = require('fs');
const _ = require('lodash');

const rawInput = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');

const lineRegex = /(\d+),(\d+) -> (\d+),(\d+)/;
const lines = rawInput.map(x => { 
    const [_all, x1, y1, x2, y2] = x.match(lineRegex).map(x => parseInt(x));
    return [{x: x1, y: y1}, {x: x2, y: y2}];
});

const bounds = {
    x: _.max(_.flatten(lines).map(p => p.x)),
    y: _.max(_.flatten(lines).map(p => p.y))
};

const grid = _.range(0, bounds.x).map(() => _.range(0, bounds.y).map(() => 0));

const verticalLinePoints = lines
    .filter(([p1, p2]) => p1.y === p2.y)
    .map(([p1, p2]) => _.range(_.min([p1.x, p2.x]), _.max([p1.x, p2.x]) + 1).map(x => ({x, y: p1.y})) );
const horizontalLinePoints = lines
    .filter(([p1, p2]) => p1.x === p2.x)
    .map(([p1, p2]) => _.range(_.min([p1.y, p2.y]), _.max([p1.y, p2.y]) + 1).map(y => ({x: p1.x, y})) );

const points = _.flatten(verticalLinePoints.concat(horizontalLinePoints));
for (let point of points)
    grid[point.x][point.y]++;

//console.log(grid);
console.log(_.flatten(grid).filter(x => x > 1).length);