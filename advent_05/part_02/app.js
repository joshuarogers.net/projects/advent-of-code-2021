const fs = require('fs');
const _ = require('lodash');

const rawInput = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');

const lineRegex = /(\d+),(\d+) -> (\d+),(\d+)/;
const lines = rawInput.map(x => { 
    const [_all, x1, y1, x2, y2] = x.match(lineRegex).map(x => parseInt(x));
    return [{x: x1, y: y1}, {x: x2, y: y2}];
});

const bounds = {
    x: _.max(_.flatten(lines).map(p => p.x)),
    y: _.max(_.flatten(lines).map(p => p.y))
};

const grid = _.range(0, bounds.x + 1).map(() => _.range(0, bounds.y + 1).map(() => 0));

const rasterizeLine = ([{x: x1, y: y1}, {x: x2, y: y2}]) => {
    const stepCount = Math.max(Math.abs(x2 - x1), Math.abs(y2 - y1));
    const dy = (y2 - y1) / stepCount;
    const dx = (x2 - x1) / stepCount;

    return _.range(0, stepCount + 1).map(i => ({x: x1 + (dx * i), y: y1 + (dy * i)}));
}

const points = _(lines).map(rasterizeLine).flatten().value();
for (let point of points)
    grid[point.x][point.y]++;

console.log(_.flatten(grid).filter(x => x > 1).length);