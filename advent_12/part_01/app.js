const fs = require('fs');
const _ = require('lodash');

const edges = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .map(x => x.split('-'));

// Build the graph
const nodes = _(edges)
    .flatten()
    .uniq()
    .map(x => ({
        id: x,
        allowRevisiting: x.toLowerCase() !== x,
        links: []
    }))
    .map(x => [x.id, x])
    .fromPairs()
    .value();

for (const [id1, id2] of edges) {
    nodes[id1].links.push(nodes[id2]);
    nodes[id2].links.push(nodes[id1]);
}

const findAllPaths = graph => {
    const findAllPathsInner = path => {
        const currentNode = _.last(path);

        if (currentNode.id === 'end')
            return [ path ];

        const potentialNeighborVisits = currentNode.links
            .filter(x => path.indexOf(x) === -1 || x.allowRevisiting);

        return _(potentialNeighborVisits)
            .map(x => findAllPathsInner(path.concat([x])))
            .flatten()
            .value();
    }

    return findAllPathsInner([graph['start']]);
}

console.log(findAllPaths(nodes).length);