const fs = require('fs');

const inputLines = fs.readFileSync('input.txt').toString().trim().split('\n');
const inputCommands = inputLines.map(x => {
    const [direction, amount] = x.split(' ');
    return [direction, parseInt(amount)]
});

const startingPosition = {
    horizontal: 0,
    depth: 0,
    aim: 0
};

const commandLookup = {
    'forward': (state, amount) => ({ ...state,
        horizontal: state.horizontal += amount,
        depth: state.depth += state.aim * amount
    }),
    'down': (state, amount) => ({ ...state, aim: state.aim += amount }),
    'up': (state, amount) => ({ ...state, aim: state.aim -= amount })
};

const finalPosition = inputCommands.reduce((position, [direction, amount]) => {
    return commandLookup[direction](position, amount);
}, startingPosition);

const finalOutput = finalPosition.horizontal * finalPosition.depth;
console.log(finalOutput);