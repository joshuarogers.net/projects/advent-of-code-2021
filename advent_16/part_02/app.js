const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt')
    .toString()
    .trim()
    .split('')
    .map(x => parseInt(x,  16).toString(2).padStart(4, '0'))
    .join('');

const createReader = input => {
    let position = 0;

    const read = count => {
        const result = position < input.length
            ? input.slice(position, position + count)
            : null;

        position += count;
        return result;
    }

    return { read, position: () => position };
};

// Parser
const readInt = (reader, bitCount) => {
    const bits = reader.read(bitCount);
    return parseInt(bits, 2);
};

const decodeValuePacket = numberReader => {
    const isNotLastPacket = 1

    const nibbles = []
    while (numberReader.read(1) === isNotLastPacket)
        nibbles.push(numberReader.read(4));
    nibbles.push(numberReader.read(4));

    const value = nibbles.reduce((result, current) => (result * 16) + current, 0);
    return { value };
}

const decodeOperatorPacket = numberReader => {
    const readSubpacketCount = count => _.range(0, count)
        .map(() => decodePacket(numberReader));

    const readSubpacketBits = bitLength => {
        const subpacketsEndPosition = numberReader.position() + bitLength;
        
        const subpackets = [];
        while (subpacketsEndPosition > numberReader.position())
            subpackets.push(decodePacket(numberReader));
        return subpackets;
    }

    const readSubpackets = () => {
        const isLengthUnitBits = numberReader.read(1) === 0;
        const subpackets = isLengthUnitBits
            ? readSubpacketBits(numberReader.read(15))
            : readSubpacketCount(numberReader.read(11));
        return { subpackets };
    }

    return readSubpackets();
};

const decodePacket = numberReader => {
    const version = numberReader.read(3);
    const type =  numberReader.read(3);

    const body = type === 4
        ? decodeValuePacket(numberReader)
        : decodeOperatorPacket(numberReader);

    return { ...body, version, type }
}

// Processor
const OPCODES = {
    SUM: 0,
    PRODUCT: 1,
    MIN: 2,
    MAX: 3,
    GREATERTHAN: 5,
    LESSTHAN: 6,
    EQUALS: 7
}

const operations = {
    [OPCODES.SUM]: values => _.sum(values),
    [OPCODES.PRODUCT]: values => values.reduce((x, y) => x * y, 1),
    [OPCODES.MIN]: values => _.min(values),
    [OPCODES.MAX]: values => _.max(values),
    [OPCODES.GREATERTHAN]: ([v1, v2]) => v1 > v2 ? 1 : 0,
    [OPCODES.LESSTHAN]: ([v1, v2]) => v1 < v2 ? 1 : 0,
    [OPCODES.EQUALS]: ([v1, v2]) => v1 === v2
};

const evaluatePacket = packet => packet.value !== undefined
    ? packet.value
    : operations[packet.type](packet.subpackets.map(evaluatePacket));

// Runner
const inputReader = createReader(input);
const numberReader = { read: bitCount => readInt(inputReader, bitCount), position: inputReader.position };
const packetTree = decodePacket(numberReader);
const result = evaluatePacket(packetTree);

console.log(result);
