const fs = require('fs');
const _ = require('lodash');

const input = fs.readFileSync('input.txt').toString().trim().split('\n');
const height = input.length;
const width = input[0].length;

const isValidCoordinate = ([x, y]) => x >= 0 && y >= 0 && x < width && y < height;
const getIndexFromCoordinate = ([x, y]) => y * width + x;
const getCoordinateFromIndex = i => [i % width, Math.floor(i / width)];
const getRelativeCoordinate = ([x, y], [dx, dy]) => [x + dx, y + dy];

const unpassableWallHeight = '9';
const unfilledCellIndices = new Set(_(input.join(""))
    .map((height, i) => height !== unpassableWallHeight ? i : null)
    .filter(x => x !== null)
    .value());

const isCellUnprocessed = coordinate => unfilledCellIndices.has(getIndexFromCoordinate(coordinate));
const hasUnfilledCells = () => !unfilledCellIndices.values().next().done;
const getNextUnfilledCell = () => getCoordinateFromIndex(unfilledCellIndices.values().next().value);
const fillCell = coordinate => unfilledCellIndices.delete(getIndexFromCoordinate(coordinate));

const floodFill = coordinate => {
    const floodCells = [];
    const floodQueue = [coordinate];

    while (floodQueue.length > 0) {
        const currentCell = floodQueue.pop();

        if (isValidCoordinate(currentCell) && isCellUnprocessed(currentCell)) {
            fillCell(currentCell);

            floodCells.push(currentCell);
            floodQueue.push(
                getRelativeCoordinate(currentCell, [1,  0]),
                getRelativeCoordinate(currentCell, [-1, 0]),
                getRelativeCoordinate(currentCell, [0,  1]),
                getRelativeCoordinate(currentCell, [0, -1])
            );
        }
    }

    return floodCells;
};

const pocketSizes = [];
while (hasUnfilledCells()) {
    const floodedCells = floodFill(getNextUnfilledCell());
    pocketSizes.push(floodedCells.length);
}

const pocketSizesDesc = _.orderBy(pocketSizes, x => x, "desc");
console.log(pocketSizesDesc[0] * pocketSizesDesc[1] * pocketSizesDesc[2]);
