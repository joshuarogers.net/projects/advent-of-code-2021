const fs = require('fs');

const input = fs.readFileSync('input.txt').toString().trim().split('\n');

const height = input.length;
const width = input[0].length;

const getHeight = (x, y) => parseInt(input[y][x]);

let sum = 0;

for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
        const here = getHeight(x, y)
        const lowerThanWest = x === 0 || here < getHeight(x - 1, y);
        const lowerThanEast = x === width - 1 || here < getHeight(x + 1, y);
        const lowerThanNorth = y === 0 || here < getHeight(x, y - 1);
        const lowerThanSouth = y === height - 1 || here < getHeight(x, y + 1);

        if (lowerThanEast && lowerThanNorth && lowerThanSouth && lowerThanWest)
            sum += (here + 1);
    }
}

console.log(sum);